<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('cars')->group(function () {
    Route::get('/', 'CarController@index')->name('car.index');

    Route::get('{id}', 'CarController@show')->name('car.show');
});

Route::prefix('admin')->group(function () {
    Route::resource('cars', 'CarController', ['except' => [
        'create', 'edit',
    ]]);
});
