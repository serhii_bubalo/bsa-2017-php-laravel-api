<?php

namespace App\Http\Controllers;

use App\Car;
use Illuminate\Http\Request;
use App\Repositories\Contracts\CarRepositoryInterface;

class CarController extends Controller
{
    private $carRepository;

    public function __construct(CarRepositoryInterface $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = $this->carRepository->getAll();

        $data = $cars->map(function ($car) {
            return collect($car)
                ->only(['id', 'model', 'year', 'color', 'price'])
                ->all();
        });

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newCar = new Car($request->all());

        return response()->json($this->carRepository->store($newCar));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $car = $this->carRepository->getById($id);

        return $car ? response()->json($car) : response()->json(['error' => "There is no car with id $id"], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return mixed
     */
    public function update(Request $request, int $id)
    {
        $car = $this->carRepository->getById($id);

        if ($car) {
            $car->fromArray($request->all());
        }

        return $car ? response()->json($this->carRepository->update($car)) : response()->json(['error' => "There is no car with id $id"], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        $car = $this->carRepository->getById($id);

        $collection = $this->carRepository->delete($id);

        return $car ? $collection : response()->json(['error' => "There is no car with id $id"], 404);
    }
}
